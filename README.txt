
-- SUMMARY --

Translation module provides entity/field translation for new translatable fields
capability in Drupal 7.

For a full description of the module, visit the project page:
  http://drupal.org/project/translation

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/translation


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* @todo


-- USAGE --

* @todo


-- CONTACT --

Current maintainers:
* Francesco Placella (plach) - http://drupal.org/user/183211
* Daniel F. Kudwien (sun) - http://drupal.org/user/54136

